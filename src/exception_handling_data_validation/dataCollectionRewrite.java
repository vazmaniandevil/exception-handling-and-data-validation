package exception_handling_data_validation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;

public class dataCollectionRewrite {
    //local class variables that can be accessed from any method
    static int inputOne;
    static int inputTwo;
    static Collection<Integer> output;

    public static void main(String[] args) {
        //While loop when user types 0 on inputOne
        // or inputTwo
        while (inputOne == 0 || inputTwo == 0) {
            //Create a Scanner object
            Scanner myScanner = new Scanner(System.in);

            //Prompt user for first number
            System.out.println("Enter First Number");
            inputOne = myScanner.nextInt(); //Read user input

            //Prompt user for second number
            System.out.println("Enter Second Number"); //Prompt user
            inputTwo = myScanner.nextInt(); //Read user input

            //Try and Catch exception handling
            try {
                //Calls the calculate method and outputs it
                output = calculate();
                System.out.println("The answer is: " + output);
            }   catch (Exception e) {
                System.out.println("Do not use number 0. Please, try again!");
            }   finally {
                System.out.println("The 'try catch' is finished.");
            }
        }
    }

    //Calculate method
    public static Collection<Integer> calculate() throws Exception {
        //ArrayList Collection
        List<Integer> listOne = new ArrayList<>();
        listOne.add(inputOne + inputTwo);
        listOne.add(inputOne / inputTwo);
        listOne.add(inputOne * inputTwo);
        listOne.add(inputOne - inputTwo);

        //Use IF statement in case user inputs number 0
        if (inputTwo == 0) {
            throw new Exception();
        }
        if (inputOne == 0) {
            throw new Exception();
        }
        //Otherwise return simple calculation
        else {
            return listOne;
        }
    }
}
