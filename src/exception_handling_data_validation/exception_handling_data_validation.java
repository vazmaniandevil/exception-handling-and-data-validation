package exception_handling_data_validation;

import java.util.Scanner;  //Import the Scanner class

public class exception_handling_data_validation {

        //local class variables that can be accessed from any method
        static int inputOne;
        static int inputTwo;
        static int output;

    public static void main(String[] args) {
        //While loop until user inputs a number different than 0
        while (inputTwo == 0) {
            //Create a Scanner object
            Scanner myScanner = new Scanner(System.in);

            //Prompt user for first number
            System.out.println("Enter First Number");
            inputOne = myScanner.nextInt(); //Read user input

            //Prompt user for second number
            System.out.println("Enter Second Number"); //Prompt user
            inputTwo = myScanner.nextInt(); //Read user input

            //Try and Catch exception handling
            try {
                //Calls the calculate method and outputs it
                output = calculate();
                System.out.println("The answer is: " + output);
            }   catch (Exception e) {
                System.out.println("Second number can't be a 0. Please, try again!");
            }   finally {
                System.out.println("The 'try catch' is finished.");
            }
        }
    }

    //Calculate method
    public static int calculate() throws Exception {
        //Use IF statement in case user inputs number o
        if (inputTwo == 0) {
            throw new Exception();
        }
        //Otherwise return simple calculation
        else {
            return inputOne / inputTwo;
        }
    }
}
